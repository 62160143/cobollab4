       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Bmi.
       AUTHOR. Supakit.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT   PIC 999V99.
       01  HEIGHT   PIC 999V99 .
       01  BMI      PIC 99V99.
       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "INPUT YOUR HEIGHT (cm.) - " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           DISPLAY "INPUT YOUR WEIGHT (kg.) - " WITH NO ADVANCING 
           ACCEPT WEIGHT 
           COMPUTE BMI = WEIGHT / (HEIGHT * 0.01)**2
           END-COMPUTE 
           DISPLAY "BMI Result : " BMI
           IF BMI < 18.5 THEN
              DISPLAY "DETAIL : UNDERWEIGHT"
           END-IF 
           IF BMI >= 18.5 AND BMI <= 24.9
              DISPLAY "DETAIL : NORMAL"
           END-IF 
           IF BMI >= 25.0 AND BMI <= 29.9
              DISPLAY "DETAIL : OVERWEIGHT"
           END-IF 
           IF BMI >= 30.0 AND BMI <= 34.9
              DISPLAY "DETAIL : OBESITY"
           END-IF 
           IF BMI >=35.0
              DISPLAY "DETAIL : EXTREMLY OBESITY"
           END-IF 
       .

